#!/usr/bin/env python

import json
from pprint import pprint
import docx
from docx import Document
from docx.shared import Inches
from docx.enum.dml import MSO_THEME_COLOR_INDEX

def add_hyperlink(paragraph, url, text):
    """
    A function that places a hyperlink within a paragraph object.

    :param paragraph: The paragraph we are adding the hyperlink to.
    :param url: A string containing the required url
    :param text: The text displayed for the url
    :return: The hyperlink object
    """

    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element
    new_run = docx.oxml.shared.OxmlElement('w:r')

    # Create a new w:rPr element
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)
    
    r = paragraph.add_run()
    r._r.append(hyperlink)

    r.font.color.theme_color = MSO_THEME_COLOR_INDEX.HYPERLINK
    r.font.underline = True
    return hyperlink

fig = 0
def figCount():
    global fig
    fig = fig + 1
    return fig

tab = 0
def tabCount():
    global tab
    tab = tab + 1
    return tab

def heading(text, lvl):
    l = [lvl][0]
    hdr = document.add_heading(text, level=l)
    return hdr

with open('facts.json') as f:
    data = json.load(f)

tmosV = str(data["system_info"]["product_version"])
tmosB = str(data["system_info"]["product_build"])


model = {'BIG-IP Virtual Edition': 'virtual.png',
        'i5000': 'i5000.png'
        }  

#set image
img = str(data["system_info"]["marketing_name"])
pic = "roles/facts/files/"+(model.get(img))

d=[]
int=[]
#print str(data["vlans"][1]["name"])
#print str(data["vlans"][1]["interfaces"][0]["name"])
#print str(data["virtual_servers"][0]["destination_port"])
#print len(data["vlans"])
#create dictionary of vlans and interfaces
for t in range(len(data["vlans"])):
    for v in range(1):
        d.append(str(data["vlans"][t]["full_path"]))
        d.append(str(data["vlans"][t]["interfaces"][0]["name"]))
        d.append(str(data["vlans"][t]["tag"]))
        int.append(d)
        d = []    


#turn this to def for self ips
a = []
m = []
for i in range(len(data["self_ips"])):
    for j in range(1):
        a.append(str(data["self_ips"][i]["vlan"]))
        a.append(str(data["self_ips"][i]["name"]))
        a.append(str(data["self_ips"][i]["address"])+"/"+str(data["self_ips"][i]["netmask_cidr"]))
        a.append(str(data["self_ips"][i]["floating"]))
        m.append(a)
        a = []


l = []

for sl in m:
    for n in range(len(int)):
        if int[n][0] in sl:
            sl.insert(0, int[n][1])
    l.append(sl) 

s = []
p = []
for c in range(len(data["virtual_servers"])):
    for j in range(1):
        s.append(str(data["virtual_servers"][c]["full_path"]))
        s.append(str(data["virtual_servers"][c]["destination_address"]))
        s.append(str(data["virtual_servers"][c]["destination_port"]))
        try:
            s.append(str(data["virtual_servers"][c]["default_pool"]))
        except KeyError:
            s.append('none')
        p.append(s)
        s = []
#print p


pl = []
pm = []
#print len(data["ltm_pools"][0]["members"])
for pp in range(len(data["ltm_pools"])):
    for pd in range(len(data["ltm_pools"][pp]["members"])):
        pl.append(str(data["ltm_pools"][pp]["full_path"]))
        pl.append(str(data["ltm_pools"][pp]["members"][pd]["name"]))
        pl.append(str(data["ltm_pools"][pp]["lb_method"]))
        pm.append(pl)
        pl = []

pa = []
pt = []
for ra in range(len(data["partitions"])):
    for n in range(1):
        pa.append(str(data["partitions"][ra]["name"]))
        pa.append(str(data["partitions"][ra]["default_route_domain"]))
        try:
            pa.append(str(data["partitions"][ra]["description"]))
        except KeyError:
            pa.append('none')
        pt.append(pa)
        pa = []

document = Document('template6.docx')

heading('System Build Info', 1)
heading('System has software and build versions', 2)
document.add_paragraph('TMOS: '+tmosV+' Build: '+tmosB)
paragraph = document.add_paragraph(style='ImgC')
run = paragraph.add_run()
picture = run.add_picture(pic, width=Inches(1.25))
document.add_paragraph('Figure '+str(figCount())+': '+img, style='CapF')

document.add_paragraph()

table = document.add_table(rows=1, cols=5, style='Light Grid Accent 6')
#table.style='LightShadingAccent1'
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Interface'
hdr_cells[1].text = 'VLAN'
hdr_cells[2].text = 'Name'
hdr_cells[3].text = 'Self IP'
hdr_cells[4].text = 'Floating'
for int, vlan, name, sip, flt in l:
   row_cells = table.add_row().cells
   row_cells[0].text = str(int)
   row_cells[1].text = vlan
   row_cells[2].text = name
   row_cells[3].text = str(sip)
   row_cells[4].text = flt
document.add_paragraph('Table '+str(tabCount())+': Network', style='CapT')

#document.add_heading('User Partitions', level=2)
heading('User Partition', 2)

document.add_paragraph('An administrative partition is a logical container that you create, containing a defined set of BIG-IP system objects. If you have the Administrator or User Manager user role assigned to the BIG-IP system user account, you can create administrative partitions to control other users access to BIG-IP objects. More specifically, when a specific set of objects resides in a partition, you can give certain users the authority to view and manage the objects in that partition only, rather than to all objects on the BIG-IP system. This gives a finer granularity of administrative control.', style='norm')

document.add_paragraph()

tP = document.add_table(rows=1, cols=3, style='Light Grid Accent 6')
hdrP_cells = tP.rows[0].cells
hdrP_cells[0].text = 'Parition'
hdrP_cells[1].text = 'Route Domain'
hdrP_cells[2].text = 'Description'
for part, rd, desc in pt:
   rowP_cells = tP.add_row().cells
   rowP_cells[0].text = part
   rowP_cells[1].text = rd
   rowP_cells[2].text = desc
document.add_paragraph('Table '+str(tabCount())+': Partition', style='CapT')


document.add_page_break()

#document.add_heading('Virtual Servers', level=2)
heading('Virtual Servers', 2)
document.add_paragraph('Configured Virtual Servers are listed below in the table by:')
document.add_paragraph('Virtual Server Name', style='Orange')
document.add_paragraph('Virtual Server IP Address', style='Orange')
document.add_paragraph('Virtual Server Port', style='Orange')
document.add_paragraph('Associated Pool', style='Orange')

document.add_paragraph('A virtual server is one of the most important components of any BIG-IP Local Traffic ManagerTM configuration. When you configure a virtual server, you create two Local Traffic Manager objects: a virtual server and a virtual address. \n \
What is a virtual server? \
A virtual server is a traffic-management object on the BIG-IP system that is represented by an IP address and a service. Clients on an external network can send application traffic to a virtual server, which then directs the traffic according to your configuration instructions. The main purpose of a virtual server is often to balance traffic load across a pool of servers on an internal network. Virtual servers increase the availability of resources for processing client requests. \n \
Not only do virtual servers distribute traffic across multiple servers, they also treat varying types of traffic differently, depending on your traffic-management needs. For example, a virtual server can enable compression on HTTP request data as it passes through the BIG-IP system, or decrypt and re-encrypt SSL connections and verify SSL certificates. For each type of traffic, such as TCP, UDP, HTTP, SSL, SIP, and FTP, a virtual server can apply an entire group of settings, to affect the way that Local Traffic Manager manages that traffic type.')

document.add_paragraph()

t2 = document.add_table(rows=1, cols=4, style='Light Grid Accent 6')
hdr2_cells = t2.rows[0].cells
hdr2_cells[0].text = 'Virtual Server'
hdr2_cells[1].text = 'Listener'
hdr2_cells[2].text = 'Port'
hdr2_cells[3].text = 'Pool'
for ser, list, port, pool in p:
   row2_cells = t2.add_row().cells
   row2_cells[0].text = str(ser)
   row2_cells[1].text = str(list)
   row2_cells[2].text = str(port)
   row2_cells[3].text = str(pool)
document.add_paragraph('Table '+str(tabCount())+': Virtual Servers', style='CapT')

document.add_page_break()

#document.add_heading('Pools', level=2)
heading('Pools', 2)

document.add_paragraph('A load balancing pool is a logical set of devices, such as web servers, that you group together to receive and process traffic. Instead of sending client traffic to the destination IP address specified in the client request, Local Traffic Manager sends the request to any of the servers that are members of that pool. This helps to efficiently distribute the load on your server resources. \
When you create a pool, you assign pool members to the pool. A pool member is a logical object that represents a physical node (server), on the network. You then associate the pool with a virtual server on the BIG-IP system. Once you have assigned a pool to a virtual server, Local Traffic Manager directs traffic coming into the virtual server to a member of that pool. An individual pool member can belong to one or multiple pools, depending on how you want to manage your network traffic. \n \
The specific pool member to which Local Traffic Manager chooses to send the request is determined by the load balancing method that you have assigned to that pool. A load balancing method is an algorithm that Local Traffic Manager uses to select a pool member for processing a request. For example, the default load balancing method is Round Robin, which causes Local Traffic Manager to send each incoming request to the next available member of the pool, thereby distributing requests evenly across the servers in the pool.')

document.add_paragraph()

t3 = document.add_table(rows=1, cols=3, style='Light Grid Accent 6')
hdr3_cells = t3.rows[0].cells
hdr3_cells[0].text = 'Pool Name'
hdr3_cells[1].text = 'Members'
hdr3_cells[2].text = 'LB Method'

for pool, member, lb in pm:
   row3_cells = t3.add_row().cells
   row3_cells[0].text = str(pool)
   row3_cells[1].text = str(member)
   row3_cells[2].text = lb

document.add_paragraph('Table '+str(tabCount())+': Pools', style='CapT')

document.add_page_break()

#document.add_heading('F5 Hardening Practices', level=2)
heading('F5 Hardening Practices', 2)
h0 = document.add_paragraph('You can restrict access to the configuration utility by source IP address by modifying the database variables in `sys httpd` outlined in this K article ')
add_hyperlink(h0, 'https://support.f5.com/csp/article/K13309', 'K13309')
h0.add_run('. By default all are allowed.')

h1 = document.add_paragraph('Blocking ssh access (F5 strongly recommends you do this via the Configuration Utility)  ')
add_hyperlink(h1, 'https://support.f5.com/csp/article/K5380', 'K5380')

document.add_page_break()
#document.add_heading('Support Links', level=2)
heading('Support Links', 2)
document.add_paragraph('Items listed below for maintenance and support')

p0 = document.add_paragraph('Opening a new case with F5 Technical support ', style='Orange')
add_hyperlink(p0, 'https://support.f5.com/csp/article/K623', 'K623')

p11 = document.add_paragraph('Generating diagnostic data using qkview utility ', style='Orange')
add_hyperlink(p11, 'https://support.f5.com/csp/article/K12878', 'K12878')

p2 = document.add_paragraph('Overview of packet tracing with tcpdump ', style='Orange')
add_hyperlink(p2, 'https://support.f5.com/csp/article/K411', 'K411')

p1 = document.add_paragraph('Platform Lifecycle Support ', style='Orange')
add_hyperlink(p1, 'https://support.f5.com/csp/article/K4309', 'K4309')

p2 = document.add_paragraph('Software Support ', style='Orange')
add_hyperlink(p2, 'https://support.f5.com/csp/article/K5903', 'K5903')

p3 = document.add_paragraph('Software Lifecycle Policy ', style='Orange')
add_hyperlink(p3, 'https://support.f5.com/csp/article/K8986', 'K8986')

p4 = document.add_paragraph('Changing System Maintenance Account Passwords ', style='Orange')
add_hyperlink(p4, 'https://support.f5.com/csp/article/K13121', 'K13121')

p5 = document.add_paragraph('Configuring the BIG-IP system to log TCP RST packets ', style='Orange')
add_hyperlink(p5, 'https://support.f5.com/csp/article/K13223', 'K13223')

p6 = document.add_paragraph('vCMP guest memory/cpu core allocation ', style='Orange')
add_hyperlink(p6, 'https://support.f5.com/csp/article/K14218', 'K14218')

document.save('f5.docx')

